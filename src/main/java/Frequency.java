import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class Frequency {

    private final String fileName = "text";
    private final String delimitersInText = "[\\s+., ]+";
    private final int descendingOrder = -1;
    private final int quantityMostFrequencyWords = 2;

    public void calculateFrequency() {

        List<Map.Entry<String, Integer>> sortedWordsList =
                new ArrayList<>(countWordsAndQuantity(fileName).entrySet());
        sortedWordsList.sort(new Comparator<Map.Entry<String, Integer>>() {
            @Override
            public int compare(Map.Entry<String, Integer> a, Map.Entry<String, Integer> b) {
                return descendingOrder * a.getValue().compareTo(b.getValue());
            }
        });

        mostFrequencyWords(sortedWordsList, quantityMostFrequencyWords);
    }

    private Map<String, Integer> countWordsAndQuantity(String fileName) {

        Map<String, Integer> listWordsAndQuantity = new HashMap<>();
        File fileWithText = new File(fileName);
        try (Scanner scanner = new Scanner(fileWithText)) {
            while (scanner.hasNext()) {
                String word = scanner.useDelimiter(delimitersInText).next();
                Integer count = listWordsAndQuantity.get(word);
                if (count == null) {
                    count = 0;
                }
                listWordsAndQuantity.put(word, ++count);
            }
        } catch (FileNotFoundException exception){
            exception.getMessage();
        }
        return listWordsAndQuantity;
    }

    private void mostFrequencyWords(List<Map.Entry<String, Integer>> sortedWordsList, int quantityMostFrequencyWords) {

        for (int i = 0; i < quantityMostFrequencyWords; i++) {
            System.out.println(sortedWordsList.get(i).getKey() + " -> " + sortedWordsList.get(i).getValue());
        }
    }
}
