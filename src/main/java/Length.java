import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class Length {

    private final String fileName = "text";
    private final String delimitersInText = "[\\s+., ]+";
    private final int quantityLongestWords = 3;

    public void calculateLength() {

        String[] arrayWordsFromText = splitTextOnWords(fileName, quantityLongestWords);
        String[] arraySortWordsByLength = sortWordsByLength(arrayWordsFromText);
        printLongestWords(arraySortWordsByLength, quantityLongestWords);
    }

    private String[] splitTextOnWords(String fileName, int quantityLongestWords) {

        String[] wordsFromText = new String[quantityLongestWords];
        File fileWithText = new File(fileName);

        try (Scanner scanner = new Scanner(fileWithText)) {
            wordsFromText = scanner.nextLine().split(delimitersInText);
        } catch (FileNotFoundException exception){
            exception.getMessage();
        }
        return wordsFromText;
    }

    private String[] sortWordsByLength(String[] wordsFromText) {

        Arrays.sort(wordsFromText, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return Integer.compare(o2.length(), o1.length());
            }
        });
        return wordsFromText;
    }

    private void printLongestWords(String[] wordsFromText, int quantityLongestWords) {

        for (int i = 0; i < quantityLongestWords; i++) {
            System.out.println(wordsFromText[i] + " -> " + wordsFromText[i].length());
        }
    }
}
