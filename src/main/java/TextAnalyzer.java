import java.util.*;

public class TextAnalyzer  {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        /* Enter the operation from the keyboard */
        System.out.println("Enter the operation: frequency / length / duplicates");
        String c = in.next();

        /* Making an operation */
        switch (c) {
            case "frequency":
                new Frequency().calculateFrequency();
                break;
            case "length":
                new Length().calculateLength();
                break;
            case "duplicates":
                new Duplicates().calculateDuplicates();
                break;
            default:
                System.out.println("You entered wrong operation!");
        }
    }
}

