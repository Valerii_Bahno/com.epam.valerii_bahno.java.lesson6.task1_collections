import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class Duplicates {

    private final String fileName = "text";
    private final String delimitersInText = "[\\s+., ]+";
    private final int duplicatesWordsQuantity = 3;

    public void calculateDuplicates() {

        // Count duplicates, select it, sort by length, reverse words and display it
        Map<String, Integer> mapDuplicatesWords = countDuplicatesWords(fileName);
        ArrayList<String> selectDuplicates = selectDuplicateWords(mapDuplicatesWords);
        String[] sortDuplicates = sortWordsByLength(selectDuplicates, duplicatesWordsQuantity);
        String[] reverseDuplicates = reverseWords(sortDuplicates);

        for (String item: reverseDuplicates) {
            System.out.println(item);
        }
    }

    private Map<String, Integer> countDuplicatesWords(String fileName) {

        Map<String, Integer> wordsCount = new HashMap<>();
        File fileWithText = new File(fileName);
        try (Scanner scanner = new Scanner(fileWithText)) {
            String[] words = scanner.nextLine().split(delimitersInText);

            // Split text on words and count it duplicates in text
            for (String str : words) {
                if (!wordsCount.containsKey(str)) {
                    wordsCount.put(str, 1);
                } else {
                    wordsCount.put(str, wordsCount.get(str) + 1);
                }
            }
        } catch (FileNotFoundException exception){
            exception.getMessage();
        }
        return wordsCount;
    }

    private ArrayList<String> selectDuplicateWords(Map<String, Integer> wordsCount) {

        ArrayList<String> duplicateWords = new ArrayList<>();
        for (Map.Entry<String, Integer> item : wordsCount.entrySet()) {
            if (item.getValue() > 1)
                duplicateWords.add(item.getKey());
        }
        return duplicateWords;
    }

    private String[] sortWordsByLength(ArrayList<String> duplicateWords, int duplicatesWordsQuantity) {

        String[] duplicatesWords = new String[duplicatesWordsQuantity];
        for (int i = 0; i < duplicatesWords.length; i++) {
            duplicatesWords[i] = duplicateWords.get(i);
        }
        Arrays.sort(duplicatesWords, Collections.reverseOrder());
        return duplicatesWords;
    }

    private String[] reverseWords(String[] duplicatesWords) {

        for (int i = 0; i < duplicatesWords.length; i++) {
            StringBuilder stringBuilder = new StringBuilder(duplicatesWords[i].toUpperCase());
            duplicatesWords[i] = String.valueOf(stringBuilder.reverse());
        }
        return duplicatesWords;
    }
}
